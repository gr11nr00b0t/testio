package task.tesonet.tesonet.utils.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerModel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("distance")
    @Expose
    private int distance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

}
