package task.tesonet.tesonet.repository.communication.servers;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import task.tesonet.tesonet.MyApp;
import task.tesonet.tesonet.repository.SharedPreferencesManager;
import task.tesonet.tesonet.repository.communication.retrofit.RetrofitClient;
import task.tesonet.tesonet.repository.communication.servers.interfaces.OnFinishedListener;
import task.tesonet.tesonet.utils.models.ServerModel;

public class ServersListImpl implements ServersListInteractor {

    @Override
    public void getList(final OnFinishedListener listener) {
        Call<List<ServerModel>> call = RetrofitClient.getInstance().getApiService().getServers(SharedPreferencesManager.getInstance(MyApp.getContext()).getToken());
        call.enqueue(new Callback<List<ServerModel>>() {
            @Override
            public void onResponse(Call<List<ServerModel>> call, final Response<List<ServerModel>> response) {
                if (response.body() != null) {
                    listener.onFinishedGetList(response.body());
                } else {
                    listener.onFailedGetList("Server Error");
                }
            }

            @Override
            public void onFailure(Call<List<ServerModel>> call, Throwable t) {
                listener.onFailedGetList(t.toString());
            }
        });
    }
}
