package task.tesonet.tesonet.repository.communication.tokens.interfaces;

public interface OnFinishedListener extends OnFinishedGetToken, OnFailedGetToken {

}
