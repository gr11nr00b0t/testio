package task.tesonet.tesonet.repository.communication.servers;


import task.tesonet.tesonet.repository.communication.servers.interfaces.OnFinishedListener;

public interface ServersListInteractor {
    void getList(OnFinishedListener listener);
}
