package task.tesonet.tesonet.repository.communication.tokens;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import task.tesonet.tesonet.MyApp;
import task.tesonet.tesonet.repository.SharedPreferencesManager;
import task.tesonet.tesonet.repository.communication.retrofit.RetrofitClient;
import task.tesonet.tesonet.repository.communication.tokens.interfaces.OnFinishedListener;
import task.tesonet.tesonet.utils.models.Authorization;
import task.tesonet.tesonet.utils.models.TokenModel;

public class TokensImpl implements TokensInteractor {

    @Override
    public void getList(final OnFinishedListener listener, Authorization authorization) {
        Call<TokenModel> call = RetrofitClient.getInstance().getApiService().getToken(authorization);
        call.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if (response.body() != null) {
                    SharedPreferencesManager.getInstance(MyApp.getContext()).setToken(response.body().getToken());
                    listener.onFinishedGetToken();
                } else {
                    listener.onFailedGetToken("Server Error");
                }
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                listener.onFailedGetToken(t.toString());
            }
        });
    }
}
