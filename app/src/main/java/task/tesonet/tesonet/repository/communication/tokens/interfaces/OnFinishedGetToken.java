package task.tesonet.tesonet.repository.communication.tokens.interfaces;

public interface OnFinishedGetToken {
    void onFinishedGetToken();
}
