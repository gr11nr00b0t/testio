package task.tesonet.tesonet.repository.communication.tokens;


import task.tesonet.tesonet.repository.communication.tokens.interfaces.OnFinishedListener;
import task.tesonet.tesonet.utils.models.Authorization;

public interface TokensInteractor {
    void getList(OnFinishedListener listener, Authorization authorization);
}
