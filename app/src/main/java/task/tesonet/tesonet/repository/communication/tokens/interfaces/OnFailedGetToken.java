package task.tesonet.tesonet.repository.communication.tokens.interfaces;

public interface OnFailedGetToken {
    void onFailedGetToken(String message);
}
