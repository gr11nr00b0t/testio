package task.tesonet.tesonet.repository.communication.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import task.tesonet.tesonet.utils.models.Authorization;
import task.tesonet.tesonet.utils.models.ServerModel;
import task.tesonet.tesonet.utils.models.TokenModel;

public interface ApiService {

    @GET("servers")
    Call<List<ServerModel>> getServers(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json"})
    @POST("tokens")
    Call<TokenModel> getToken(@Body Authorization authorization);
}
