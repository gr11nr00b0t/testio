package task.tesonet.tesonet.repository.communication.servers.interfaces;

import java.util.List;

import task.tesonet.tesonet.utils.models.ServerModel;

public interface OnFinishedGetList {
    void onFinishedGetList(List<ServerModel> list);
}
