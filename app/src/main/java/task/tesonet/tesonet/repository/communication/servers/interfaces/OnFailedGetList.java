package task.tesonet.tesonet.repository.communication.servers.interfaces;

public interface OnFailedGetList {
    void onFailedGetList(String message);
}
