package task.tesonet.tesonet.aplication.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import task.tesonet.tesonet.R;
import task.tesonet.tesonet.aplication.main.ActivityMain;
import task.tesonet.tesonet.utils.models.Authorization;
import task.tesonet.tesonet.utils.views.MyTextInputLayout;

public class ActivityLogin extends MvpAppCompatActivity implements ViewLogin {

    @InjectPresenter
    PresenterLogin mPresenter;

    private MyTextInputLayout til_user_name, til_password;
    private TextInputEditText et_user_name, et_password;
    private TextView tv_login;
    private ProgressBar progress;
    private FrameLayout fl_logo;
    private ImageView iv_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_login);

        iv_logo = findViewById(R.id.iv_logo);
        Animation animLogo = AnimationUtils.loadAnimation(this, R.anim.logo_animation);
        til_user_name = findViewById(R.id.til_user_name);
        fl_logo = findViewById(R.id.fl_logo);
        fl_logo.setAnimation(animLogo);
        progress = findViewById(R.id.progress);
        til_password = findViewById(R.id.til_password);
        et_user_name = findViewById(R.id.et_user_name);
        et_user_name.setText("tesonet");
        et_password = findViewById(R.id.et_password);
        et_password.setText("partyanimal");
        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_user_name.getText().toString().length() == 0 || et_password.getText().toString().length() == 0) {
                    if (et_user_name.getText().toString().length() == 0) {
                        til_user_name.setError(" ");
                    }
                    if (et_password.getText().toString().length() == 0) {
                        til_password.setError(" ");
                    }
                } else {
                    TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 100);
                    animation.setInterpolator(new BounceInterpolator());
                    animation.setFillAfter(true);
                    animation.setDuration(300);
                    progress.setVisibility(View.VISIBLE);
                    iv_logo.setAnimation(null);
                    iv_logo.startAnimation(animation);
                    tv_login.setClickable(false);
                    mPresenter.auth(new Authorization(et_user_name.getText().toString(), et_password.getText().toString()));
                }
            }
        });



    }

    @Override
    public void onFailedGetToken(final String message) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TranslateAnimation animation = new TranslateAnimation(0, 0, 100, 0);
                animation.setFillAfter(true);
                animation.setDuration(300);
                progress.setVisibility(View.INVISIBLE);
                iv_logo.setAnimation(null);
                iv_logo.startAnimation(animation);
                tv_login.setClickable(true);
            }
        }, 300);

    }

    @Override
    public void onFinishedGetToken() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(ActivityLogin.this, ActivityMain.class));
                ActivityLogin.this.finish();
            }
        }, 300);

    }
}
