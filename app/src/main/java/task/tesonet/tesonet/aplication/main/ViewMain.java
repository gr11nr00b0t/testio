package task.tesonet.tesonet.aplication.main;

import com.arellomobile.mvp.MvpView;

import task.tesonet.tesonet.repository.communication.servers.interfaces.OnFailedGetList;
import task.tesonet.tesonet.repository.communication.servers.interfaces.OnFinishedGetList;

interface ViewMain extends MvpView, OnFinishedGetList, OnFailedGetList {

}
