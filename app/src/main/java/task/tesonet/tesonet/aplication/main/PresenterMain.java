package task.tesonet.tesonet.aplication.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import task.tesonet.tesonet.repository.communication.servers.ServersListImpl;
import task.tesonet.tesonet.repository.communication.servers.ServersListInteractor;
import task.tesonet.tesonet.repository.communication.servers.interfaces.OnFinishedListener;
import task.tesonet.tesonet.utils.models.ServerModel;

@InjectViewState
public class PresenterMain extends MvpPresenter<ViewMain> implements OnFinishedListener {

    private ServersListInteractor serversListInteractor;

    PresenterMain() {
        serversListInteractor = new ServersListImpl();
    }

    public void getList() {
        serversListInteractor.getList(this);
    }

    @Override
    public void onFailedGetList(String message) {
        getViewState().onFailedGetList(message);
    }

    @Override
    public void onFinishedGetList(List<ServerModel> list) {
        getViewState().onFinishedGetList(list);
    }
}
