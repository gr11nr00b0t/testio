package task.tesonet.tesonet.aplication.login;

import com.arellomobile.mvp.MvpView;

import task.tesonet.tesonet.repository.communication.tokens.interfaces.OnFailedGetToken;
import task.tesonet.tesonet.repository.communication.tokens.interfaces.OnFinishedGetToken;

interface ViewLogin extends MvpView, OnFinishedGetToken, OnFailedGetToken {

}
