package task.tesonet.tesonet.aplication.login;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import task.tesonet.tesonet.repository.communication.tokens.TokensImpl;
import task.tesonet.tesonet.repository.communication.tokens.TokensInteractor;
import task.tesonet.tesonet.repository.communication.tokens.interfaces.OnFinishedListener;
import task.tesonet.tesonet.utils.models.Authorization;

@InjectViewState
public class PresenterLogin extends MvpPresenter<ViewLogin> implements OnFinishedListener {

    private TokensInteractor tokensInteractor;

    PresenterLogin() {
        tokensInteractor = new TokensImpl();
    }

    public void auth(Authorization authorization) {
        tokensInteractor.getList(this, authorization);
    }

    @Override
    public void onFailedGetToken(String message) {
        getViewState().onFailedGetToken(message);
    }

    @Override
    public void onFinishedGetToken() {
        getViewState().onFinishedGetToken();
    }
}