package task.tesonet.tesonet.aplication.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import task.tesonet.tesonet.R;
import task.tesonet.tesonet.utils.models.ServerModel;

public class AdapterServers extends RecyclerView.Adapter<AdapterServers.ViewHolder> {

    private Context context;
    private List<ServerModel> list;

    AdapterServers(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    @NonNull
    @Override
    public AdapterServers.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.i_server, parent, false);
        return new AdapterServers.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterServers.ViewHolder holder, final int position) {

        final ServerModel model = list.get(position);
        String name = "Name: " + model.getName();
        String distance = "Distance: " + String.valueOf(model.getDistance());
        holder.tv_name.setText(name);
        holder.tv_distance.setText(distance);
        holder.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Do some with " + model.getName(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_distance;
        private LinearLayout ll_click;

        ViewHolder(View itemView) {
            super(itemView);
            ll_click = itemView.findViewById(R.id.ll_click);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_distance = itemView.findViewById(R.id.tv_distance);
        }
    }

    public void addItems(List<ServerModel> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyItemRangeInserted(0, list.size());
    }
}
