package task.tesonet.tesonet.aplication.main;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import task.tesonet.tesonet.R;
import task.tesonet.tesonet.utils.models.ServerModel;

public class ActivityMain extends MvpAppCompatActivity implements ViewMain {

    @InjectPresenter
    PresenterMain mPresenter;
    private AdapterServers adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        overridePendingTransition(R.anim.main_exit, android.R.anim.fade_out);

        adapter = new AdapterServers(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setItemAnimator(new SlideInRightAnimator(new OvershootInterpolator(0.1f)));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        recyclerView.setAdapter(adapter);
        mPresenter.getList();
    }

    @Override
    public void onFailedGetList(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFinishedGetList(final List<ServerModel> list) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.addItems(list);
            }
        }, 300);
    }
}