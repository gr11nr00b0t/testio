# Newspaper
Simple example client-server aplication

Get news from http://newsapi.org/ and show it by category

Aplication on Google Play http://play.google.com/store/apps/details?id=newspaper.gamestudiostandart.newspaper

# Code summary
- Architectral patter(MVP) by Moxy
- Reqwest to server(Retrofit2, Retrofit2:converter-gson)
- Caching (SQLite)
- LOGS (com.squareup.okhttp3:logging-interceptor:...)
- Two libraries for refresh Bounce and Bounce on list news efect

# Screen Capture

![](https://media.giphy.com/media/yuQvSAdfVbNUiPJZBl/giphy.gif) ![](http://media.giphy.com/media/YWWgtGkP2KWVlsTpfr/giphy.gif)

Two libraries for refresh Bounce and Bounce on list news efect

![](http://media.giphy.com/media/kERJqKjDrnxTjaH83y/giphy.gif) ![](http://media.giphy.com/media/lffWSl65jOQyRPKuta/giphy.gif)

